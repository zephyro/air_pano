<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Главная</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>360° Видео AirPano</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">
            <div class="container">
                <h1>360° Панорамы AirPano</h1>

                <div class="display-bar clearfix">
                    <div class="show-bar clearfix">
                        <div class="display-label">Показывать:</div>
                        <div class="show-bar-nav">
                            <ul class="drop-list clearfix">
                                <li class="sort-mosaic"><a href="#">мозаикой</a></li>
                                <li class="sort-gallery"><a href="#">галереей</a></li>
                                <li class="sort-list active"><a href="#">списком</a></li>
                                <li class="sort-map"><a href="#">на карте</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sort-nav clearfix">
                        <div class="display-label">Выводить по:</div>
                        <div class="sort-select">
                            <select name="sort">
                                <option value="">популярности</option>
                                <option value="">публикации</option>
                                <option value="">алфавиту</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="album-gallery">

                    <div class="album-image-inner">

                        <div class="album-image">

                            <a href="images/gallery_album_01.jpg" class="album-image-item btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/gallery_album_01.jpg" alt="" class="img-responsive">
                                <div class="album-image-text">
                                    <div class="album-list-name">Рио-де-Жанейро, Бразилия</div>
                                    <div class="album-list-id">ID 1223</div>
                                </div>
                            </a>

                            <a href="images/gallery_album_02.jpg" class="album-image-item btn-modal-image" data-fancybox="gallery"  data-caption="<div class='album-list-name'>Нью-Йoрк, США. Утро после шторма</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/gallery_album_02.jpg" alt="" class="img-responsive">
                                <div class="album-image-text">
                                    <div class="album-list-name">Нью-Йoрк, США. Утро после шторма</div>
                                    <div class="album-list-id">ID 1223</div>
                                </div>
                            </a>

                            <a href="images/gallery_album_01.jpg" class="album-image-item btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/gallery_album_01.jpg" alt="" class="img-responsive">
                                <div class="album-image-text">
                                    <div class="album-list-name">Рио-де-Жанейро, Бразилия</div>
                                    <div class="album-list-id">ID 1223</div>
                                </div>
                            </a>

                            <a href="images/gallery_album_02.jpg" class="album-image-item btn-modal-image" data-fancybox="gallery"  data-caption="<div class='album-list-name'>Нью-Йoрк, США. Утро после шторма</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/gallery_album_02.jpg" alt="" class="img-responsive">
                                <div class="album-image-text">
                                    <div class="album-list-name">Нью-Йoрк, США. Утро после шторма</div>
                                    <div class="album-list-id">ID 1223</div>
                                </div>
                            </a>

                            <a href="images/gallery_album_01.jpg" class="album-image-item btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/gallery_album_01.jpg" alt="" class="img-responsive">
                                <div class="album-image-text">
                                    <div class="album-list-name">Рио-де-Жанейро, Бразилия</div>
                                    <div class="album-list-id">ID 1223</div>
                                </div>
                            </a>

                            <a href="images/gallery_album_02.jpg" class="album-image-item btn-modal-image" data-fancybox="gallery"  data-caption="<div class='album-list-name'>Нью-Йoрк, США. Утро после шторма</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/gallery_album_02.jpg" alt="" class="img-responsive">
                                <div class="album-image-text">
                                    <div class="album-list-name">Нью-Йoрк, США. Утро после шторма</div>
                                    <div class="album-list-id">ID 1223</div>
                                </div>
                            </a>

                            <a href="images/gallery_album_01.jpg" class="album-image-item btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/gallery_album_01.jpg" alt="" class="img-responsive">
                                <div class="album-image-text">
                                    <div class="album-list-name">Рио-де-Жанейро, Бразилия</div>
                                    <div class="album-list-id">ID 1223</div>
                                </div>
                            </a>

                            <a href="images/gallery_album_02.jpg" class="album-image-item btn-modal-image" data-fancybox="gallery"  data-caption="<div class='album-list-name'>Нью-Йoрк, США. Утро после шторма</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/gallery_album_02.jpg" alt="" class="img-responsive">
                                <div class="album-image-text">
                                    <div class="album-list-name">Нью-Йoрк, США. Утро после шторма</div>
                                    <div class="album-list-id">ID 1223</div>
                                </div>
                            </a>


                        </div>

                        <span class="gallery-nav prev"><i class="fa fa-angle-left"></i></span>
                        <span class="gallery-nav next"><i class="fa fa-angle-right"></i></span>

                    </div>

                    <div class="album-thumbs">
                        <div class="album-thumbs-item">
                            <img src="images/gallery_album_01_thumb.jpg" alt="">
                        </div>
                        <div class="album-thumbs-item">
                            <img src="images/gallery_album_02_thumb.jpg" alt="">
                        </div>
                        <div class="album-thumbs-item">
                            <img src="images/gallery_album_01_thumb.jpg" alt="">
                        </div>
                        <div class="album-thumbs-item">
                            <img src="images/gallery_album_02_thumb.jpg" alt="">
                        </div>
                        <div class="album-thumbs-item">
                            <img src="images/gallery_album_01_thumb.jpg" alt="">
                        </div>
                        <div class="album-thumbs-item">
                            <img src="images/gallery_album_02_thumb.jpg" class="img-responsive" alt="">
                        </div>
                        <div class="album-thumbs-item">
                            <img src="images/gallery_album_01_thumb.jpg" alt="">
                        </div>
                        <div class="album-thumbs-item">
                            <img src="images/gallery_album_02_thumb.jpg" alt="">
                        </div>
                    </div>

                </div>


            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
