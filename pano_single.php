<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <section class="single">
            <!-- Pagination -->
            <div class="pagination">
                <div class="container-fluid">
                    <div class="pagination-back clearfix">
                        <a href="#">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <span>Грозный</span>
                    </div>

                    <ul class="pagination-nav">
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">360° Видео</a></li>
                        <li><a href="#">Весь мир</a></li>
                        <li><a href="#">Восточная Азия</a></li>
                        <li><a href="#">Китай</a></li>
                        <li>Национальный парк Чжанцзяцзе (горы Аватар)</li>
                    </ul>
                </div>
            </div><!-- -->

            <div class="single-map">
                <div id="map"></div>
                <div class="single-map-top">
                    <a href="#" class="btn-map"><span class="map-show">Показать</span><span class="map-hide">Свернуть</span></a>
                </div>
            </div>

            <div class="container">


                <div class="single-video">
                    <img src="images/pano.jpg" class="img-responsive" alt="">
                </div>

                <div class="single-nav">
                    <div class="single-action">
                        <ul class="social-group clearfix">
                            <li><a href="#" class="social-fb"></a></li>
                            <li><a href="#" class="social-vk"></a></li>
                            <li><a href="#" class="social-tw"></a></li>
                            <li><a href="#" class="social-intagramm"></a></li>
                            <li><a href="#" class="social-per"></a></li>
                        </ul>
                        <button class="btn">Купить видео</button>
                    </div>

                    <div class="single-banner">
                        <a href="#">
                            <img src="images/abanner.png" class="img-responsive" alt="">
                        </a>
                    </div>
                </div>


                <div class="rows">
                    <div class="content-left">
                        <div class="single-content">
                            <h1>Национальный парк Чжанцзяцзе (горы Аватар)</h1>

                            <p>Неизвестно, как скоро открылась бы миру красота «Чжанцзяцзе», если бы не фильм Джеймса Кэмерона «Аватар». Его действие разворачивается на вымышленном небесном теле Пандора. И, хотя съемки велись не в Китае, считается, что именно пейзажи национального парка «Чжанцзяцзе» вдохновили знаменитого режиссера.</p>
                            <p>И вроде бы это — старейшая из природоохранных территорий Китая (с 1982 года), добраться сюда легко из любого большого аэропорта — но не посмотри я фильм Аватар (а после него парк получил неофициальное название «парк Аватар»), боюсь, я бы его даже не нашел, планируя путешествие и съемки в Китайской Народной Республике.</p>
                            
                            <img src="images/single-img_3.jpg" class="img-responsive" alt="">

                            <p>Парк — это, конечно, не совсем Пандора с ее летающими скалами, заросшими лианами и водопадами, а также драконами. Но если мысленно перевернуть картинку увиденного на 180 градусов, то сходство становится очевидным.</p>


                            <div class="content-hide">
                                <div class="content-hide-inner">
                                    <p>Неизвестно, как скоро открылась бы миру красота «Чжанцзяцзе», если бы не фильм Джеймса Кэмерона «Аватар». Его действие разворачивается на вымышленном небесном теле Пандора. И, хотя съемки велись не в Китае, считается, что именно пейзажи национального парка «Чжанцзяцзе» вдохновили знаменитого режиссера.</p>
                                    <p>И вроде бы это — старейшая из природоохранных территорий Китая (с 1982 года), добраться сюда легко из любого большого аэропорта — но не посмотри я фильм Аватар (а после него парк получил неофициальное название «парк Аватар»), боюсь, я бы его даже не нашел, планируя путешествие и съемки в Китайской Народной Республике.</p>
                                    <div class="text-right">Фото: <a href="#">Станислав Седов</a> и <a href="#">Дмитрий Моисеенко</a> 09.08.2016</div>
                                </div>
                                <div class="text-center">
                                    <a href="#" class="btn btn-md btn-text-show">Читать дальше</a>
                                </div>
                            </div>
                        </div>

                        <div class="single-gallery">
                            <div class="h1">Фотогалерея</div>

                            <div class="flexbin flexbin-margin">
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img01.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img02.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img03.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img04.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img05.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img06.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img07.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img08.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img09.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                            </div>

                        </div>

                        <div class="single-pano">
                            <div class="h1">360° Видео</div>

                            <ul class="gallery-table gallery-video clearfix">

                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_video_01.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Венецианский карнавал. Часть 1</span>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_video_02.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Водопад Виктория, Замбия - Зимбабве. Часть 1</span>
                                        </div>
                                    </a>
                                </li>

                            </ul>

                        </div>

                    </div>

                    <div class="side-right">
                        <aside class="single-side">
                            <h4>Наша планета в 360°</h4>

                            <ul class="side-gallery">
                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_img_01.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Бутан. Часть 2. Монастырь Тхангби Лхакханг</span>
                                        </div>
                                        <span class="icon-pano"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="images/side-banner.jpg" class="img-responsive" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_video_05.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Водопад Виктория, Замбия - Зимбабве. Часть 1</span>
                                        </div>
                                        <span class="icon-video"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_img_03.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Сенот Эль-Пит, Мексика</span>
                                        </div>
                                        <span class="icon-pano"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_img_04.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Рафтинг на реке Замбези, Замбия-Зимбабве</span>
                                        </div>
                                        <span class="icon-pano"></span>
                                    </a>
                                </li>
                            </ul>
                            <a href="#" class="btn">Смотреть еще</a>
                        </aside>
                    </div>
                </div>

                <div class="single-review">

                    <div class="h1">Оставьте отзыв о панораме Национального парка Чжанцзяцзе (горы Аватар), Китай</div>
                    <div class="rows">
                        <div class="side-left">
                            <div class="auth">
                                <div class="auth-social">
                                    <span class="auth-label">Авторизуйтесь:</span>
                                    <ul class="social-group clearfix">
                                        <li><a href="#" class="social-fb"></a></li>
                                        <li><a href="#" class="social-vk"></a></li>
                                        <li><a href="#" class="social-tw"></a></li>
                                    </ul>
                                </div>
                                <p>или укажите информацию о себе:</p>
                                <div class="auth-form">
                                    <form class="form">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="name" placeholder="Имя">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="place" placeholder="Город и страна">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="message" placeholder="Текст сообщения"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-send">Отправить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="content-right">
                            <ul class="messages">
                                <li>
                                    <div class="message-avatar">
                                        <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                    </div>
                                    <div class="message-text">
                                        <p>Эх, жаль панорамы Припяти нет. А могло бы интересно получиться. Тем более, что скоро её совсем уже не будет.</p>
                                        <span class="message-meta">21 июля 2016, <span>Алексей Махматов, Россия</span></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="message-avatar">
                                        <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                    </div>
                                    <div class="message-text">
                                        <p>Добрый день, почему вы не заливаете 360 видео на ютьюб? Там навигация инверсная и есть возможность просматривать видео через cardboard.</p>
                                        <span class="message-meta">21 июля 2016, <span>Руслан, Украина</span></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="message-avatar">
                                        <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                    </div>
                                    <div class="message-text">
                                        <p>Как отписаться от рассылки?</p>
                                        <span class="message-meta">10 июля 2016, <span>H. Shvarz</span></span>
                                    </div>

                                    <ul>
                                        <li>
                                            <div class="message-avatar">
                                                <img src="images/guest-avatar_02.png" class="img-responsive" alt="">
                                            </div>
                                            <div class="message-text">
                                                <p>В конце каждого письма есть ссылка, по которой надо перейти, чтобы отписаться от рассылки</p>
                                                <span class="message-meta">AirPano</span>
                                            </div>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <div class="message-avatar">
                                        <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                    </div>
                                    <div class="message-text">
                                        <p>Добрый день, почему вы не заливаете 360 видео на ютьюб? Там навигация инверсная и есть возможность просматривать видео через cardboard.</p>
                                        <span class="message-meta">21 июля 2016, <span>Руслан, Украина</span></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="bottom-banner">
                    <a href="#">
                        <img src="images/bottom-banner.jpg" class="img-responsive" alt="">
                    </a>
                </div>

            </div>

        </section>



        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
