<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Главная</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>Карта съемок AirPano</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">

            <div class="container">
                <h1>Карта съемок AirPano</h1>
            </div>

            <div class="gallery-map">
                <div class="map-content-inner">
                    <div class="container">
                        <div class="map-content">
                            <a href="#" class="map-image">
                                <img src="images/home_video_03.jpg" alt="" class="img-responsive">
                                <div class="map-content-text">
                                    <span>Тбилиси, Грузия</span>
                                </div>
                            </a>
                            <span class="map-content-close"></span>
                        </div>
                    </div>
                </div>
                <div id="map"></div>
            </div>

        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
