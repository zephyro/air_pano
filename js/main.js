

$(".btn-modal-image").fancybox({

    baseClass : 'image-view-layout',
    margin    : 0,
    infobar   : true,

    buttons : true,
    slideShow  : false,
    fullScreen : false,
    thumbs     : false,

    touch : {
        vertical : 'auto'
    },
    baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
    '<div class="fancybox-bg"></div>' +
    '<button data-fancybox-close class="image-view-close" title="Close (Esc)"></button>' +
    '<button data-fancybox-previous class="image-nav-button prev" title="Previous"><i class="fa fa-angle-left"></i></button>' +
    '<button data-fancybox-next class="image-nav-button next" title="Next"><i class="fa fa-angle-right"></i></button>' +
    '<div class="image-view-action"> ' +
        '<ul class="social-group clearfix">' +
            '<li><a href="#" class="social-fb"></a></li>' +
            '<li><a href="#" class="social-vk"></a></li>' +
            '<li><a href="#" class="social-tw"></a></li>' +
            '<li><a href="#" class="social-intagramm"></a></li>' +
            '<li><a href="#" class="social-per"></a></li>' +
        '</ul>' +

        '<button class="btn btn-md">Купить фото</button> ' +
    '</div>' +

    '<div class="fancybox-controls">' +
        '<div class="fancybox-infobar">' +

        '</div>' +
        '<div class="fancybox-buttons">' +

        '</div>' +
    '</div>' +
    '<div class="fancybox-slider-wrap">' +
    '<div class="fancybox-slider"></div>' +
    '</div>' +
    '<div class="fancybox-caption-wrap"><div class="fancybox-caption album-image-text"></div></div>' +
    '</div>'

});

// Слайдер в шапке на Главной

$('.header-slider').slick({
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
});


$('.header-slider-nav.prev').click(function(){
    $('.header-slider').slick('slickPrev');
});

$('.header-slider-nav.next').click(function(){
    $('.header-slider').slick('slickNext');
});



// Слайдер на странице О нас

$('.about-slider').slick({
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
});


$('.review').slick({
    dots: false,
    arrows: true,
    loop: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slider-nav prev"></span>',
    nextArrow: '<span class="slider-nav next"></span>'
});

$('.command').slick({
    dots: false,
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '<span class="slider-nav prev"></span>',
    nextArrow: '<span class="slider-nav next"></span>',
    responsive: [

        {
            breakpoint: 1320,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1

            }
        }
    ]
});

// Галерея

$('.album-image').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.album-thumbs'
});

$('.album-thumbs').slick({
    slidesToShow: 7,
    slidesToScroll: 1,
    asNavFor: '.album-image',
    arrows: true,
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    variableWidth: true,
    prevArrow: '<span class="slider-nav prev"></span>',
    nextArrow: '<span class="slider-nav next"></span>',
    responsive: [
        {
            breakpoint: 1320,
            settings: {
                slidesToShow: 5
            }
        },
        {
            breakpoint: 982,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 610,
            settings: {
                slidesToShow: 3
            }
        }
    ]
});

$('.gallery-nav.prev').click(function(){
    $('.album-image').slick('slickPrev');
});

$('.gallery-nav.next').click(function(){
    $('.album-image').slick('slickNext');
});

// Меню

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 1320 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});


// Фильтр поиска

$(".showing input[type='radio']").ionCheckRadio();

$('.showing-current').click(function(e) {
    $(this).closest('.showing-bar').find('.showing').toggleClass('open');
});


$('.showing input[type="radio"]').change(function(e) {
    $(this).closest('.showing').removeClass('open');
    var txt = $(this).closest('label').find('.icr-text span').text();
    var num = $(this).closest('label').find('.icr-text i').text();

    console.log(txt);
    $('.showing-current span').text(txt);
    $('.showing-current i').text(num);
});



//--




$('.btn-map').click(function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $(this).closest('.single-map').toggleClass('open');
});

$('.btn-text-show').click(function(e) {
    e.preventDefault();
    $(this).closest('.content-hide').toggleClass('open');
});



$('.drop-list li a').click(function(e) {
    e.preventDefault();
    $(this).closest('ul').toggleClass('open');
});


/// Footer Navigation

$('.footer-nav-title').click(function(e) {
    e.preventDefault();
    $(this).closest('li').toggleClass('open');
});


(function($){
    $(window).on("load",function(){

        $(".preview-gallery").mCustomScrollbar({
            axis:"x",
            theme:"inset",
            scrollInertia:950,
            advanced:{
                autoExpandHorizontalScroll:true
            },
            scrollButtons:{
                enable:true,
                scrollType:"stepless"
            }
        });
    });
})(jQuery);


// Галерея показ текста

$('.gallery-content-show').click(function(e) {
    e.preventDefault();
    $(this).hide();
    $(this).closest('.gallery-content').find('.gallery-content-hide').show();
});




// Google Maps

var map;
function initialize() {
    var Options = {
        center: new google.maps.LatLng(51.018277, 38.042185),
        zoom: 3,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), Options);
}
google.maps.event.addDomListener(window, 'load', initialize);