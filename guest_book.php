<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Главная</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>Гостевая книга</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">

            <div class="container">
                <h1>Гостевая книга</h1>
                <div class="rows">
                    <div class="side-left">
                        <div class="auth">
                            <div class="auth-social">
                                <span class="auth-label">Авторизуйтесь:</span>
                                <ul class="social-group clearfix">
                                    <li><a href="#" class="social-fb"></a></li>
                                    <li><a href="#" class="social-vk"></a></li>
                                    <li><a href="#" class="social-tw"></a></li>
                                </ul>
                            </div>
                            <p>или укажите информацию о себе:</p>
                            <div class="auth-form">
                                <form class="form">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="name" placeholder="Имя">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="place" placeholder="Город и страна">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="email" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-12">
                                            <div class="form-group">
                                                <textarea class="form-control" name="message" placeholder="Текст сообщения"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-send">Отправить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="content-right">
                        <ul class="messages">
                            <li>
                                <div class="message-avatar">
                                    <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                </div>
                                <div class="message-text">
                                    <p>Эх, жаль панорамы Припяти нет. А могло бы интересно получиться. Тем более, что скоро её совсем уже не будет.</p>
                                    <span class="message-meta">21 июля 2016, <span>Алексей Махматов, Россия</span></span>
                                </div>
                            </li>
                            <li>
                                <div class="message-avatar">
                                    <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                </div>
                                <div class="message-text">
                                    <p>Добрый день, почему вы не заливаете 360 видео на ютьюб? Там навигация инверсная и есть возможность просматривать видео через cardboard.</p>
                                    <span class="message-meta">21 июля 2016, <span>Руслан, Украина</span></span>
                                </div>
                            </li>
                            <li>
                                <div class="message-avatar">
                                    <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                </div>
                                <div class="message-text">
                                    <p>Как отписаться от рассылки?</p>
                                    <span class="message-meta">10 июля 2016, <span>H. Shvarz</span></span>
                                </div>

                                <ul>
                                    <li>
                                        <div class="message-avatar">
                                            <img src="images/guest-avatar_02.png" class="img-responsive" alt="">
                                        </div>
                                        <div class="message-text">
                                            <p>В конце каждого письма есть ссылка, по которой надо перейти, чтобы отписаться от рассылки</p>
                                            <span class="message-meta">AirPano</span>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <div class="message-avatar">
                                    <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                </div>
                                <div class="message-text">
                                    <p>Добрый день, почему вы не заливаете 360 видео на ютьюб? Там навигация инверсная и есть возможность просматривать видео через cardboard.</p>
                                    <span class="message-meta">21 июля 2016, <span>Руслан, Украина</span></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>


        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
