<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Новости</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>Контакты</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">

            <div class="container">

                <div class="price-block">

                    <h1 class="heading"><span class="color-green">Прайс-лист</span></h1>

                    <div class="heading-text">Оплата возможна онлайн-платежом на PayPal или по заключению договора с ИП, обслуживающим продажу контента AirPano. После совершения платежа итоговый цифровой файл загружается на FTP для скачивания</div>

                    <ul class="price">
                        <li>
                            <div class="price-item">
                                <h3>Файлы для печати</h3>
                                <div class="price-value">от 10 000 руб.</div>
                                <div class="price-text">Фотографии или нарезка из сферических панорам <a href="#">(примеры)</a>.  Предоставляется в форматах TIFF или JPEG с минимальным сжатием.</div>
                                <a href="#" class="btn btn-md">Подробнее</a>
                            </div>
                        </li>
                        <li>
                            <div class="price-item">
                                <h3>Панорамы</h3>
                                <div class="price-value">от 21 000 руб.</div>
                                <div class="price-text">Полные сферические панорамы 360х180. Стоимость приобретения панорамы зависит от категории места съемки.</div>
                                <a href="#" class="btn btn-md">Подробнее</a>
                            </div>
                        </li>
                        <li>
                            <div class="price-item">
                                <h3>Видео</h3>
                                <div class="price-value">от 1 500 руб.</div>
                                <div class="price-text">Стоимость видеофайла зависит от категории и указывается за 1 секунду.</div>
                                <a href="#" class="btn btn-md">Подробнее</a>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="contact-form">
                    <div class="contact-title">Хотите что-либо приобрести? Остались вопросы? Пишите!</div>
                    <div class="contact-body">
                        <ul class="form-group">
                            <li>
                                <label class="form-label">Ваше имя:</label>
                            </li>
                            <li>
                                <input type="text" class="form-control" name="name" placeholder="Имя">
                            </li>
                        </ul>
                        <ul class="form-group">
                            <li>
                                <label class="form-label">Ваш телефон:</label>
                            </li>
                            <li>
                                <input type="text" class="form-control" name="phone" placeholder="+_(___)_______">
                            </li>
                        </ul>
                        <ul class="form-group">
                            <li>
                                <label class="form-label">Email:</label>
                            </li>
                            <li>
                                <input type="text" class="form-control" name="email" placeholder="Email">
                            </li>
                        </ul>
                        <ul class="form-group">
                            <li>
                                <label class="form-label">Сообщение:</label>
                            </li>
                            <li>
                                <textarea class="form-control" name="message" placeholder="Текст сообщения" rows="5"></textarea>
                            </li>
                        </ul>
                        <div class="clearfix">
                            <button type="submit" class="btn btn-send">Отправить</button>
                        </div>
                    </div>
                </div>

            </div>
        </section>


        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
