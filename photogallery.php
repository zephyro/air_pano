<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Главная</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>Фотогалерея</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">
            <div class="container">
                <h1>Фотогалерея</h1>

                <div class="display-bar clearfix">
                    <div class="sort-nav clearfix">
                        <div class="display-label">Выводить по:</div>
                        <div class="sort-select">
                            <select name="sort">
                                <option value="">популярности</option>
                                <option value="">публикации</option>
                                <option value="">алфавиту</option>
                            </select>
                        </div>
                    </div>
                </div>

                <ul class="photo-gallery clearfix">

                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>105</b> фото</span>
                            <img src="images/photo_01.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Лучшие фото AirPano</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>54</b> фото</span>
                            <img src="images/photo_02.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Лучшие фото AirPano.Планеты</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>48</b> фото</span>
                            <img src="images/photo_03.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Панорамы высокого разрешения</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>32</b> фото</span>
                            <img src="images/photo_04.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Панорамы высокого разрешения</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>160</b> фото</span>
                            <img src="images/photo_05.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Лучшие фото AirPano</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>75</b> фото</span>
                            <img src="images/photo_06.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Лучшие фото AirPano.Планеты</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>105</b> фото</span>
                            <img src="images/photo_01.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Лучшие фото AirPano</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>54</b> фото</span>
                            <img src="images/photo_02.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Лучшие фото AirPano.Планеты</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>48</b> фото</span>
                            <img src="images/photo_03.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Панорамы высокого разрешения</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>32</b> фото</span>
                            <img src="images/photo_04.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Панорамы высокого разрешения</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>160</b> фото</span>
                            <img src="images/photo_05.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Лучшие фото AirPano</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="photo-lnk">
                            <span class="photo-value"><b>75</b> фото</span>
                            <img src="images/photo_06.jpg" alt="" class="img-responsive">
                            <div class="photo-text">
                                <div class="photo-text-inner">
                                    <p>Лучшие фото AirPano.Планеты</p>
                                    <span>коллекция</span>
                                </div>
                            </div>
                        </a>
                    </li>

                </ul>

                <div class="ajax-load">
                    <span class="ajax-load-icon">
                        <img src="img/icon-download.png" alt="">
                    </span>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
