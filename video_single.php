<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <section class="single">
            <!-- Pagination -->
            <div class="pagination">
                <div class="container-fluid">
                    <div class="pagination-back clearfix">
                        <a href="#">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <span>Грозный</span>
                    </div>

                    <ul class="pagination-nav">
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">360° Видео</a></li>
                        <li><a href="#">Весь мир</a></li>
                        <li><a href="#">Европа</a></li>
                        <li><a href="#">Россия</a></li>
                        <li>Грозный</li>
                    </ul>
                </div>
            </div><!-- -->

            <div class="single-map">
                <div id="map"></div>
                <div class="single-map-top">
                    <a href="#" class="btn-map"><span class="map-show">Показать</span><span class="map-hide">Свернуть</span></a>
                </div>
            </div>

            <div class="container">

                <div class="rows">
                    <div class="content-left">

                        <div class="single-video">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src="https://www.youtube.com/embed/2gmQrABN7O4" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <span class="highscreen"></span>
                        </div>

                        <div class="single-nav">
                            <div class="single-action">
                                <ul class="social-group clearfix">
                                    <li><a href="#" class="social-fb"></a></li>
                                    <li><a href="#" class="social-vk"></a></li>
                                    <li><a href="#" class="social-tw"></a></li>
                                    <li><a href="#" class="social-intagramm"></a></li>
                                    <li><a href="#" class="social-per"></a></li>
                                </ul>
                                <button class="btn">Купить видео</button>
                            </div>

                            <div class="single-banner">
                                <a href="#">
                                    <img src="images/abanner.png" class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>

                        <div class="single-content">
                            <h1>Грозный</h1>

                            <p>Мы приглашаем вас в путешествие над Грозным, столицей Чеченской Республики на юге России. Основной целью нашей поездки была съемка мечети «Сердце Чечни» имени Ахмата Кадырова. Эта мечеть поразительной красоты снаружи отделана мрамором-травертином, а внутри декорирована белым мрамором, привезенным с острова Мармара Адасы в Мраморном море.</p>
                            <p>«Сердце Чечни» — самая большая мечеть в России и одна из крупнейших в Европе. Фотопанорамы этого места можно посмотреть <a href="#">здесь</a>.</p>
                            
                            <img src="images/single-img_1.jpg" class="img-responsive" alt="">

                            <img src="images/single-img_2.jpg" class="img-responsive" alt="">

                            <p>Отдельно хотим поблагодарить всех, кто оказывал нам помощь в этой поездке и в ее организации: </p>

                            <p>
                                - председателя Совета муфтиев России - Муфтия шейха Равиля Гайнутдина,<br/>
                                - заместителя председателя, руководителя аппарата совета муфтиев России - Рушана хазрата Аббясова,<br/>
                                - председателя Духовного управления мусульман Чеченской республики - Салаха хаджи Межиева,<br/>
                                - Ильдара хаджи Ямбикова.<br/>
                            </p>

                            <div class="text-right">Фото: <a href="#">Станислав Седов</a> и <a href="#">Дмитрий Моисеенко</a> 09.08.2016</div>
                        </div>

                        <div class="single-gallery">
                            <div class="h1">Фотогалерея</div>

                            <div class="flexbin flexbin-margin">
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img01.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img02.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img03.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img04.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img05.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img06.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img07.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img08.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                                <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                    <img src="images/home_gallery/img09.jpg" alt="" class="img-responsive">
                                    <div class="collage-text">
                                        <b>Центральный парк Нью-Йорк, США</b>
                                        <span>ID 5536</span>
                                    </div>
                                </a>
                            </div>

                        </div>

                        <div class="single-pano">
                            <div class="h1">360° Панорамы</div>

                            <ul class="gallery-table clearfix">

                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_img_09.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Мечеть «Сердце Чечни», Грозный, Россия</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>

                    <div class="side-right">
                        <aside class="single-side">
                            <h4>Наша планета в 360°</h4>

                            <ul class="side-gallery">
                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_img_01.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Бутан. Часть 2. Монастырь Тхангби Лхакханг</span>
                                        </div>
                                        <span class="icon-pano"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="images/side-banner.jpg" class="img-responsive" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_video_05.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Водопад Виктория, Замбия - Зимбабве. Часть 1</span>
                                        </div>
                                        <span class="icon-video"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_img_03.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Сенот Эль-Пит, Мексика</span>
                                        </div>
                                        <span class="icon-pano"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="gallery-lnk">
                                        <img src="images/home_img_04.jpg" alt="" class="img-responsive">
                                        <div class="gallery-text">
                                            <span>Рафтинг на реке Замбези, Замбия-Зимбабве</span>
                                        </div>
                                        <span class="icon-pano"></span>
                                    </a>
                                </li>
                            </ul>
                            <a href="#" class="btn">Смотреть еще</a>
                        </aside>
                    </div>
                </div>

                <div class="single-review">

                    <div class="h1">Оставьте отзыв о видео Грозный, Россия</div>
                    <div class="rows">
                        <div class="side-left">
                            <div class="auth">
                                <div class="auth-social">
                                    <span class="auth-label">Авторизуйтесь:</span>
                                    <ul class="social-group clearfix">
                                        <li><a href="#" class="social-fb"></a></li>
                                        <li><a href="#" class="social-vk"></a></li>
                                        <li><a href="#" class="social-tw"></a></li>
                                    </ul>
                                </div>
                                <p>или укажите информацию о себе:</p>
                                <div class="auth-form">
                                    <form class="form">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="name" placeholder="Имя">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="place" placeholder="Город и страна">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="message" placeholder="Текст сообщения"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-send">Отправить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="content-right">
                            <ul class="messages">
                                <li>
                                    <div class="message-avatar">
                                        <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                    </div>
                                    <div class="message-text">
                                        <p>Эх, жаль панорамы Припяти нет. А могло бы интересно получиться. Тем более, что скоро её совсем уже не будет.</p>
                                        <span class="message-meta">21 июля 2016, <span>Алексей Махматов, Россия</span></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="message-avatar">
                                        <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                    </div>
                                    <div class="message-text">
                                        <p>Добрый день, почему вы не заливаете 360 видео на ютьюб? Там навигация инверсная и есть возможность просматривать видео через cardboard.</p>
                                        <span class="message-meta">21 июля 2016, <span>Руслан, Украина</span></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="message-avatar">
                                        <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                    </div>
                                    <div class="message-text">
                                        <p>Как отписаться от рассылки?</p>
                                        <span class="message-meta">10 июля 2016, <span>H. Shvarz</span></span>
                                    </div>

                                    <ul>
                                        <li>
                                            <div class="message-avatar">
                                                <img src="images/guest-avatar_02.png" class="img-responsive" alt="">
                                            </div>
                                            <div class="message-text">
                                                <p>В конце каждого письма есть ссылка, по которой надо перейти, чтобы отписаться от рассылки</p>
                                                <span class="message-meta">AirPano</span>
                                            </div>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <div class="message-avatar">
                                        <img src="images/guest-avatar_01.png" class="img-responsive" alt="">
                                    </div>
                                    <div class="message-text">
                                        <p>Добрый день, почему вы не заливаете 360 видео на ютьюб? Там навигация инверсная и есть возможность просматривать видео через cardboard.</p>
                                        <span class="message-meta">21 июля 2016, <span>Руслан, Украина</span></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="bottom-banner">
                    <a href="#">
                        <img src="images/bottom-banner.jpg" class="img-responsive" alt="">
                    </a>
                </div>

            </div>

        </section>



        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
