<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Главная</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>360° Видео AirPano</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">
            <div class="container">
                <h1>360° Панорамы AirPano</h1>

                <div class="display-bar clearfix">
                    <div class="show-bar clearfix">
                        <div class="display-label">Показывать:</div>
                        <div class="show-bar-nav">
                            <ul class="drop-list clearfix">
                                <li class="sort-mosaic active"><a href="#">мозаикой</a></li>
                                <li class="sort-gallery"><a href="#">галереей</a></li>
                                <li class="sort-list"><a href="#">списком</a></li>
                                <li class="sort-map"><a href="#">на карте</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sort-nav clearfix">
                        <div class="display-label">Выводить по:</div>
                        <div class="sort-select">
                            <select name="sort">
                                <option value="">популярности</option>
                                <option value="">публикации</option>
                                <option value="">алфавиту</option>
                            </select>
                        </div>
                    </div>


                </div>

                <div class="flexbin flexbin-margin">
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img01.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img02.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img03.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img04.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img05.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img06.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img07.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img08.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img09.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>

                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img01.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img02.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img03.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img04.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img05.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img06.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img07.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img08.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>
                    <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                        <img src="images/home_gallery/img09.jpg" alt="" class="img-responsive">
                        <div class="collage-text">
                            <b>Центральный парк Нью-Йорк, США</b>
                            <span>ID 5536</span>
                        </div>
                    </a>

                </div>

                <div class="ajax-load">
                    <span class="ajax-load-icon">
                        <img src="img/icon-download.png" alt="">
                    </span>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
