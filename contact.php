<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Новости</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>Контакты</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">
            <div class="container">
                <h1>Контакты</h1>
                <div class="contact">

                    <div class="contact-text">
                        <h4>Информация для связи:</h4>
                        <div class="contact-name">Сергей Семенов</div>
                        <ul class="contact-list">
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <a  class="email" href="mailto:info@airpano.ru">mailto:info@airpano.ru</a></li>
                            <li>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <a class="tel" href="tel:+7(916)827-63-07">+7(916)827-63-07</a>
                                <p>(c 10:00 до 22:00 время Московское) или отправьте SMS</p>
                            </li>
                        </ul>
                    </div>

                    <div class="contact-form">
                        <div class="contact-title">Хотите что-либо приобрести? Остались вопросы? Пишите!</div>
                        <div class="contact-body">
                            <ul class="form-group">
                                <li>
                                    <label class="form-label">Ваше имя:</label>
                                </li>
                                <li>
                                    <input type="text" class="form-control" name="name" placeholder="Имя">
                                </li>
                            </ul>
                            <ul class="form-group">
                                <li>
                                    <label class="form-label">Ваш телефон:</label>
                                </li>
                                <li>
                                    <input type="text" class="form-control" name="phone" placeholder="+_(___)_______">
                                </li>
                            </ul>
                            <ul class="form-group">
                                <li>
                                    <label class="form-label">Email:</label>
                                </li>
                                <li>
                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                </li>
                            </ul>
                            <ul class="form-group">
                                <li>
                                    <label class="form-label">Сообщение:</label>
                                </li>
                                <li>
                                    <textarea class="form-control" name="message" placeholder="Текст сообщения" rows="5"></textarea>
                                </li>
                            </ul>
                            <div class="clearfix">
                                <button type="submit" class="btn btn-send">Отправить</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
