<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Новости</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>Новости</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">
            <div class="container">
                <h1>Новости</h1>

                <div class="clearfix">
                    <article class="news-content">

                        <ul class="news">
                            <li>
                                <div class="news-date">
                                    <div class="date">
                                        <span class="month">19 июля</span>
                                        <span class="year">2016</span>
                                    </div>
                                </div>
                                <div class="news-body">
                                    <h4><a href="#">Золотая черепаха 2016</a></h4>
                                    <div class="news-text">Наши фотографы в очередной раз вошли в число победителей и финалистов международного конкурса фотографий дикой природы «Золотая Черепаха 2016». Обладателем кубка и подарочной книги стал Дмитрий Моисеенко за работу в номинации «Пейзаж» — «Извержение вулкана в Ключевской сопке». А Сергей Шандин стал финалистом в номинации «Звери» c фотографией жирафа. Всех желающих приглашаем посетить выставку до 21 августа в ЦВЗ «МАНЕЖ» в Москве.</div>
                                    <div class="news-image">
                                        <a href="#">
                                            <img src="images/news_01.jpg" alt="" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="news-date">
                                    <div class="date">
                                        <span class="month">18 июля</span>
                                        <span class="year">2016</span>
                                    </div>
                                </div>
                                <div class="news-body">
                                    <h4><a href="#">Открытие выставки "Самая красивая страна"</a></h4>
                                    <div class="news-text">Сегодня руководитель проекта AirPano Сергей Семенов участвовал в открытии фотовыставки Русского Географического Общества, состоящей из работ победителей и финалистов конкурса «Самая красивая страна» 2015 года. А в 2016 году Сергей входит в жюри этого конкурса. Посмотреть фотографии можно в московском метро на станции «Выставочная» до 3 августа</div>
                                    <div class="news-image">
                                        <a href="#">
                                            <img src="images/news_02.jpg" alt="" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="news-date">
                                    <div class="date">
                                        <span class="month">17 июля</span>
                                        <span class="year">2016</span>
                                    </div>
                                </div>
                                <div class="news-body">
                                    <h4><a href="#">Семинар в рамках Российской Недели Фотографии</a></h4>
                                    <div class="news-text">Дорогие москвичи! 14 июля AirPano проведет семинар в Галерее Классической Фотографии с 14:00 до 15:30 в рамках Российской Недели Фотографии 2016. Сергей Семенов расскажет про VR, технологии 360 и дроны. Приходите, будет интересно! Билеты можно купить на сайте, также можно посмотреть <a href="#">онлайн трансляцию</a>.</div>
                                    <div class="news-image">
                                        <a href="#">
                                            <img src="images/news_03.jpg" alt="" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="news-date">
                                    <div class="date">
                                        <span class="month">16 июля</span>
                                        <span class="year">2016</span>
                                    </div>
                                </div>
                                <div class="news-body">
                                    <h4><a href="#">Российская Неделя Фотографии и конкурс 35AWARDS</a></h4>
                                    <div class="news-text">7 июля в Москве стартовала Российская Неделя Фотографии. В рамках этого события проходит выставка работ победителей конкурса <a href="#">35AWARDS</a> — 100 Best Photos of 2015, среди них представлен проект AirPano. Выставка продлится в <a href="#">Галерее Классической Фотографии</a> до 17 июля, приходите посмотреть!</div>
                                    <div class="news-image">
                                        <a href="#">
                                            <img src="images/news_04.jpg" alt="" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </article>
                    <aside class="news-sidebar">
                        <div class="news-social">
                            <div class="news-social-title">Больше интересных новостей смотрите у нас в соц. сетях</div>
                            <ul class="news-social-list clearfix">
                                <li><a href="#" class="social-fb">Facebook</a></li>
                                <li><a href="#" class="social-tw">Twitter</a></li>
                                <li><a href="#" class="social-vk">Vkontakte</a></li>
                                <li><a href="#" class="social-youtube">YouTube</a></li>
                                <li><a href="#" class="social-gplus">Google</a></li>
                                <li><a href="#" class="social-intagramm">Instagram</a></li>
                                <li><a href="#" class="social-unknown">SmugMug</a></li>
                            </ul>
                        </div>

                    </aside>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
