<footer class="footer">
    <div class="container">

        <div class="community clearfix">
            <a href="#" class="appstore">
                <img src="img/appstore.png" class="img-responsive" alt="">
            </a>
            <div class="social">
                <div class="title">Следите за нами в социальных сетях</div>
                <ul class="social-link clearfix">
                    <li><a href="#" class="social-fb"></a></li>
                    <li><a href="#" class="social-tw"></a></li>
                    <li><a href="#" class="social-vk"></a></li>
                    <li><a href="#" class="social-youtube"></a></li>
                    <li><a href="#" class="social-gplus"></a></li>
                    <li><a href="#" class="social-intagramm"></a></li>
                    <li><a href="#" class="social-unknown"></a></li>
                </ul>
            </div>
            <div class="subscribe">
                <div class="title">или подписывайтесь на наши новости</div>
                <div class="subscribe-form">
                    <form class="form">
                        <input type="text" name="search" class="navbar-input-search" placeholder="Email">
                        <button type="submit" class="btn-send">OK</button>
                    </form>
                </div>
            </div>
        </div>

        <ul class="footer-nav clearfix">
            <li>
                <div class="footer-nav-title">Информация</div>
                <ul class="footer-menu">
                    <li><a href="#">О проекте AirPano</a></li>
                    <li><a href="#">Наша команда</a></li>
                    <li><a href="#">Клиенты и партнеры</a></li>
                    <li><a href="#">Новости</a></li>
                    <li><a href="#">Статьи</a></li>
                    <li><a href="#">Вопросы и ответы</a></li>
                    <li><a href="#">Гостевая книга</a></li>
                </ul>
            </li>
            <li>
                <div class="footer-nav-title">Услуги</div>
                <ul class="footer-menu">
                    <li><a href="#">360° Панорамы</a></li>
                    <li><a href="#">360° Видео</a></li>
                    <li><a href="#">Фотографии</a></li>
                    <li><a href="#">Виртуальный тур</a></li>
                    <li><a href="#">Презентация</a></li>
                    <li><a href="#">Приложения</a></li>
                    <li><a href="#">Фотобанк</a></li>
                    <li><a href="#">Панорамный кинотеатр</a></li>
                    <li><a href="#">Сенсорная панель</a></li>
                    <li><a href="#">VR Очки</a></li>
                    <li><a href="#">Бинокуляр</a></li>
                </ul>
            </li>
            <li>
                <div class="footer-nav-title">ССЫЛКИ</div>
                <ul class="footer-menu">
                    <li><a href="#">AirPano Антарктида</a></li>
                    <li><a href="#">Промо</a></li>
                    <li><a href="#">Скачать приложение</a></li>
                </ul>
            </li>
        </ul>

        <ul class="footer-bottom clearfix">
            <li class="footer-text">
                <p>Наши панорамы и фотографии широко используются ведущими западными компаниями. Среди клиентов и партнеров AirPano – Google, Microsoft, LG, Samsung, Starbucks, Nokia и другие. Приложение AirPano Travel Book вошло в число лучших по версии Apple за 2014 год.</p>
                <a href="#">Разработка сайта: SiteSpot</a>
            </li>
            <li class="footer-contact">
                <h4>Свяжитесь с нами</h4>
                <a href="tel:+7 (916) 827-63-07" class="tel">+7 (916) 827-63-07</a>
                <br/>
                <a href="mailto:info@airpano.ru" class="email">info@airpano.ru</a>
            </li>
            <li class="footer-copy">
                <h4>AirPano</h4>
                <p>2016 © Все права защищены</p>
                <a href="#">
                    <img src="img/runet.png" class="img-responsive" alt="">
                </a>
            </li>
        </ul>

    </div>
</footer>