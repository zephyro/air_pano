<header class="header">

    <div class="container-fluid">
        <a href="#" class="header-logo">
            <img src="img/header_logo_xl.png" alt="" class="img-responsive">
        </a>
        <span class="btn-nav navbar-toggle" data-target=".navbar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>

        <nav class="navbar clearfix">
            <span class="icon-close navbar-toggle"></span>
            <div class="navbar-search hidden-lg">
                <input type="text" name="search" class="navbar-input-search" placeholder="Поиск по сайту">
                <button type="submit" class="btn-send"></button>
            </div>
            <ul class="navbar-main">
                <li class="active"><a href="#">360° Панорамы</a></li>
                <li><a href="#">360° Видео</a></li>
                <li><a href="#">Фотогалерея</a></li>
                <li><a href="#">Получить прайс</a></li>
            </ul>

            <div class="navbar-lng clearfix">
                <span class="lng-text">Выберите язык:</span>
                <ul class="lng drop-list">
                    <li class="active"><a href="#">Ru</a></li>
                    <li><a href="#">En</a></li>
                </ul>
            </div>

        </nav>

        <div class="header-search-wrap">
            <div class="header-search">
                <form class="form">
                    <input type="text" class="form-search" name="search" placeholder="Например, вулкан ...">
                    <button type="submit" class="btn-search"></button>
                </form>
            </div>
        </div>

    </div>

</header>