<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Главная</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>360° Видео AirPano</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">
            <div class="container">
                <h1>360° Панорамы AirPano</h1>

                <div class="display-bar clearfix">
                    <div class="show-bar clearfix">
                        <div class="display-label">Показывать:</div>
                        <div class="show-bar-nav">
                            <ul class="drop-list clearfix">
                                <li class="sort-table"><a href="#">таблицей</a></li>
                                <li class="sort-list active"><a href="#">списком</a></li>
                                <li class="sort-map"><a href="#">на карте</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sort-nav clearfix">
                        <div class="display-label">Выводить по:</div>
                        <div class="sort-select">
                            <select name="sort">
                                <option value="">популярности</option>
                                <option value="">публикации</option>
                                <option value="">алфавиту</option>
                            </select>
                        </div>
                    </div>
                </div>

                <ul class="gallery-list clearfix">

                    <li>
                        <a href="#" class="gallery-image">
                            <img src="images/home_img_list_01.jpg" alt="" class="img-responsive">
                        </a>
                        <div class="gallery-content">
                            <h4><a href="#">Египетские пирамиды. Часть 2</a></h4>
                            <p>Команда AirPano уже снимала пирамиды в 2011 году. Но технологии за это время улучшились, а мы стали создавать 360º видео. Поэтому мы очень хотели вновь запечатлеть великие Пирамиды Гизы.</p>
                            <a href="#" class="gallery-content-show"><span>Показать весь текст</span></a>
                            <div class="gallery-content-hide">
                                Несколько раз отправляли запросы в инстанции, пытаясь получить разрешения на съемки. Но раз за разом получали отказ: ввезти полетное и съемочное оборудование мы не можем. Что же, раз нельзя снимать с воздуха, то можно попробовать с земли. И вот я в непривычном одиночестве, без пилота дрона, отправляюсь в Каир.
                            </div>
                        </div>
                    </li>

                    <li>
                        <a href="#" class="gallery-image">
                            <img src="images/home_img_list_02.jpg" alt="" class="img-responsive">
                        </a>
                        <div class="gallery-content">
                            <h4><a href="#">Истринское водохранилище</a></h4>
                            <p>Команда AirPano уже снимала пирамиды в 2011 году. Но технологии за это время улучшились, а мы стали создавать 360º видео. Поэтому мы очень хотели вновь запечатлеть великие Пирамиды Гизы.</p>
                            <a href="#" class="gallery-content-show"><span>Показать весь текст</span></a>
                            <div class="gallery-content-hide">
                                Несколько раз отправляли запросы в инстанции, пытаясь получить разрешения на съемки. Но раз за разом получали отказ: ввезти полетное и съемочное оборудование мы не можем. Что же, раз нельзя снимать с воздуха, то можно попробовать с земли. И вот я в непривычном одиночестве, без пилота дрона, отправляюсь в Каир.
                            </div>
                        </div>
                    </li>

                    <li>
                        <a href="#" class="gallery-image">
                            <img src="images/home_img_list_03.jpg" alt="" class="img-responsive">
                        </a>
                        <div class="gallery-content">
                            <h4><a href="#">Айсберги Гренландии. Часть 4</a></h4>
                            <p>Команда AirPano уже снимала пирамиды в 2011 году. Но технологии за это время улучшились, а мы стали создавать 360º видео. Поэтому мы очень хотели вновь запечатлеть великие Пирамиды Гизы.</p>
                            <a href="#" class="gallery-content-show"><span>Показать весь текст</span></a>
                            <div class="gallery-content-hide">
                                Несколько раз отправляли запросы в инстанции, пытаясь получить разрешения на съемки. Но раз за разом получали отказ: ввезти полетное и съемочное оборудование мы не можем. Что же, раз нельзя снимать с воздуха, то можно попробовать с земли. И вот я в непривычном одиночестве, без пилота дрона, отправляюсь в Каир.
                            </div>
                        </div>
                    </li>

                    <li>
                        <a href="#" class="gallery-image">
                            <img src="images/home_img_list_04.jpg" alt="" class="img-responsive">
                        </a>
                        <div class="gallery-content">
                            <h4><a href="#">Мельницы Голландии. Часть 2</a></h4>
                            <p>Команда AirPano уже снимала пирамиды в 2011 году. Но технологии за это время улучшились, а мы стали создавать 360º видео. Поэтому мы очень хотели вновь запечатлеть великие Пирамиды Гизы.</p>
                            <a href="#" class="gallery-content-show"><span>Показать весь текст</span></a>
                            <div class="gallery-content-hide">
                                Несколько раз отправляли запросы в инстанции, пытаясь получить разрешения на съемки. Но раз за разом получали отказ: ввезти полетное и съемочное оборудование мы не можем. Что же, раз нельзя снимать с воздуха, то можно попробовать с земли. И вот я в непривычном одиночестве, без пилота дрона, отправляюсь в Каир.
                            </div>
                        </div>
                    </li>
                </ul>

                <div class="ajax-load">
                    <span class="ajax-load-icon">
                        <img src="img/icon-download.png" alt="">
                    </span>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
