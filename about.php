<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Новости</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>О проекте</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">

            <!-- About block -->
            <div class="container">
                <div class="about about-slider">

                    <div class="about-slide">
                        <div class="about-item clearfix">
                            <div class="about-content">
                                <h1>О проекте</h1>
                                <div class="text">
                                    AirPano — это некоммерческий проект, созданный командой энтузиастов, специализирующихся на панорамных фотографиях высокого разрешения, которые снимаются с воздуха.
                                </div>
                                <a href="#" class="btn">Перейти к панорамам</a>
                            </div>
                            <div class="about-image">
                                <div class="about-image-inner">
                                    <img src="images/about_slider_01.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="about-slide">
                        <div class="about-item clearfix">
                            <div class="about-content">
                                <h1>О проекте</h1>
                                <div class="text">
                                    AirPano — это некоммерческий проект, созданный командой энтузиастов, специализирующихся на панорамных фотографиях высокого разрешения, которые снимаются с воздуха.
                                </div>
                                <a href="#" class="btn">Перейти к панорамам</a>
                            </div>
                            <div class="about-image">
                                <div class="about-image-inner">
                                    <img src="images/about_slider_01.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="about-slide">
                        <div class="about-item clearfix">
                            <div class="about-content">
                                <h1>О проекте</h1>
                                <div class="text">
                                    AirPano — это некоммерческий проект, созданный командой энтузиастов, специализирующихся на панорамных фотографиях высокого разрешения, которые снимаются с воздуха.
                                </div>
                                <a href="#" class="btn">Перейти к панорамам</a>
                            </div>
                            <div class="about-image">
                                <div class="about-image-inner">
                                    <img src="images/about_slider_01.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- -->

            <div class="heading"><span>Несколько фактов об AirPano</span></div>

            <div class="container">
                <ul class="fact clearfix">
                    <li>
                        <div class="fact-value">3000</div>
                        <div class="fact-text">панорам</div>
                    </li>
                    <li>
                        <div class="fact-value">300</div>
                        <div class="fact-text">известных мест земли</div>
                    </li>
                    <li>
                        <div class="fact-value">1</div>
                        <div class="fact-text">раз в неделю выкладываем новый тур</div>
                    </li>
                    <li>
                        <div class="fact-value">360</div>
                        <div class="fact-text">угол обзора наших панорам</div>
                    </li>
                    <li>
                        <div class="fact-value">2012</div>
                        <div class="fact-text">в этом году мы стали лауреатом конкурса “Премия Рунета”</div>
                    </li>
                </ul>
            </div>
        </section>


        <section class="review-block gray-block">
            <div class="heading"><span>Отзывы</span></div>

            <div class="container">

                <div class="review">

                    <div class="review-item">
                        <div class="review-author">
                            <div class="review-photo">
                                <img src="images/review_img_01.png" class="img-responsive" alt="">
                            </div>
                            <div class="review-name">Erih M. Remark</div>
                            <div class="review-date">18 декабря 2013</div>
                        </div>
                        <div class="review-text">Здравствуйте! Огромное вам спасибо за восхитительные панорамы. Душа радуется на такую красоту смотреть. Мечтаю увидеть солончак Уюни в таком формате, надеюсь, увижу его когда-нибудь у вас</div>
                    </div>

                    <div class="review-item">
                        <div class="review-author">
                            <div class="review-photo">
                                <img src="images/review_img_01.png" class="img-responsive" alt="">
                            </div>
                            <div class="review-name">Erih M. Remark</div>
                            <div class="review-date">18 декабря 2013</div>
                        </div>
                        <div class="review-text">Здравствуйте! Огромное вам спасибо за восхитительные панорамы. Душа радуется на такую красоту смотреть. Мечтаю увидеть солончак Уюни в таком формате, надеюсь, увижу его когда-нибудь у вас</div>
                    </div>

                    <div class="review-item">
                        <div class="review-author">
                            <div class="review-photo">
                                <img src="images/review_img_01.png" class="img-responsive" alt="">
                            </div>
                            <div class="review-name">Erih M. Remark</div>
                            <div class="review-date">18 декабря 2013</div>
                        </div>
                        <div class="review-text">Здравствуйте! Огромное вам спасибо за восхитительные панорамы. Душа радуется на такую красоту смотреть. Мечтаю увидеть солончак Уюни в таком формате, надеюсь, увижу его когда-нибудь у вас</div>
                    </div>

                </div>
            </div>
        </section>

        <section class="command-block">

            <div class="heading"><span>Наша команда</span></div>
            <div class="container">
                <div class="command-text">Каждый человек в нашей команде искренне горит идеей, увлечен своей работой и очень хочет сделать этот мир чуточку прекраснее</div>

                <div class="command">
                    <div class="command-item">
                        <div class="command-image">
                            <img src="images/command_img_01.png" class="img-responsive" alt="">
                        </div>
                        <div class="command-name">Олег Гапонюк</div>
                        <div class="command-staff">Автор проекта</div>
                        <div class="command-content">
                            <p>Родился в 1970г. Живу в Москве. Закончил МИФИ по специальности инженер-     математик. Люблю фото, а особенно панорамную фотографию, горные лыжи, виндсерфинг, кайтинг, дайвинг, гитару, клавишные, PocketPC, компьютер, шутки, текилу и путешествия... Ради хорошего снимка я могу отправиться на другой конец Земли, чувство юмора помогает увидеть необычное и смешное в самых неожиданных ситуациях, ну а влияние текилы и вовсе сложно переоценить!</p>
                            <p>Мои "традиционные" фотографии можно посмотреть <a href="#">здесь</a> или <a href="#">здесь</a></p>
                            <div class="command-social">
                                <a class="fb" href="#"></a>
                                <a class="tw" href="#"></a>
                            </div>
                        </div>
                    </div>

                    <div class="command-item">
                        <div class="command-image dark">
                            <img src="images/command_img_02.png" class="img-responsive" alt="">
                        </div>
                        <div class="command-name">Андрей Зубец</div>
                        <div class="command-staff">Один из основателей проекта</div>
                        <div class="command-content">
                            <p><b>Ушел из жизни 04 сентября 2015 года.</b></p>
                            <p>Закончил Ленинградский Институт Киноинженеров в Санкт-Петербурге.Увлекался фотографией, компьютерными технологиями
                                и конструированием различных устройств. Один из пионеров российской панорамной фотографии, в частности - сферической. Написал цикл статей на русском языке с целью популяризировать этот вид творчества, находящегося на стыке технологий
                                и искусства.</p>
                        </div>
                    </div>

                    <div class="command-item">
                        <div class="command-image">
                            <img src="images/command_img_03.png" class="img-responsive" alt="">
                        </div>
                        <div class="command-name">Сергей Семенов</div>
                        <div class="command-staff">Автор проекта</div>
                        <div class="command-content">
                            <p>
                                Родился в 1983 году. Мое первое осознанное знакомство с фотографией произошло в 2003 году, когда в мои руки попала 1.3Мп цифромыльница. Возможность сразу видеть результат на экране меня просто поразила! После этого я буквально заболел фотографией и посвящал ей каждую свободную минуту.
                                <br/>
                                И хотя я и выучился с отличием на экономиста-международника и даже проработал им 7 лет, благодаря кризису, круто всё поменял: офис — на дом и поездки, костюм — на флиску, кожаный кейс — на фоторюкзак, ноутбук — на компьютер с двумя огромными мониторами, Excel и SAP — на Photoshop.
                            </p>
                            <p>Мои фотографии можно посмотреть на сайте <a href="www.SergeSemenov.com">www.SergeSemenov.com</a> и <a href="#">здесь</a></p>
                            <div class="command-social">
                                <a class="fb" href="#"></a>
                                <a class="tw" href="#"></a>
                            </div>
                        </div>
                    </div>

                    <div class="command-item">
                        <div class="command-image">
                            <img src="images/command_img_01.png" class="img-responsive" alt="">
                        </div>
                        <div class="command-name">Олег Гапонюк</div>
                        <div class="command-staff">Автор проекта</div>
                        <div class="command-content">
                            <p>Родился в 1970г. Живу в Москве. Закончил МИФИ по специальности инженер-     математик. Люблю фото, а особенно панорамную фотографию, горные лыжи, виндсерфинг, кайтинг, дайвинг, гитару, клавишные, PocketPC, компьютер, шутки, текилу и путешествия... Ради хорошего снимка я могу отправиться на другой конец Земли, чувство юмора помогает увидеть необычное и смешное в самых неожиданных ситуациях, ну а влияние текилы и вовсе сложно переоценить!</p>
                            <p>Мои "традиционные" фотографии можно посмотреть <a href="#">здесь</a> или <a href="#">здесь</a></p>
                            <div class="command-social">
                                <a class="fb" href="#"></a>
                                <a class="tw" href="#"></a>
                            </div>
                        </div>
                    </div>

                    <div class="command-item">
                        <div class="command-image">
                            <img src="images/command_img_03.png" class="img-responsive" alt="">
                        </div>
                        <div class="command-name">Сергей Семенов</div>
                        <div class="command-staff">Автор проекта</div>
                        <div class="command-content">
                            <p>
                                Родился в 1983 году. Мое первое осознанное знакомство с фотографией произошло в 2003 году, когда в мои руки попала 1.3Мп цифромыльница. Возможность сразу видеть результат на экране меня просто поразила! После этого я буквально заболел фотографией и посвящал ей каждую свободную минуту.
                                <br/>
                                И хотя я и выучился с отличием на экономиста-международника и даже проработал им 7 лет, благодаря кризису, круто всё поменял: офис — на дом и поездки, костюм — на флиску, кожаный кейс — на фоторюкзак, ноутбук — на компьютер с двумя огромными мониторами, Excel и SAP — на Photoshop.
                            </p>
                            <p>Мои фотографии можно посмотреть на сайте <a href="www.SergeSemenov.com">www.SergeSemenov.com</a> и <a href="#">здесь</a></p>
                            <div class="command-social">
                                <a class="fb" href="#"></a>
                                <a class="tw" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
