<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Главная</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>360° Видео AirPano</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">
            <div class="container">
                <h1>Результаты поиска <span>Швейцария <span class="num">(29)</span></span></h1>

                <div class="showing-bar">
                    <div class="showing-current"><span>Везде</span> <i>(29)</i></div>
                    <ul class="showing">
                        <li class="active">
                            <label class="icr-label">
                                <span class="icr-item type_radio"></span>
                                <span class="icr-hidden"><input class="icr-input" type="radio" name="show" value="0" checked/></span>
                                <span class="icr-text"><span>Везде</span> <i>(29)</i></span>
                            </label>
                        </li>
                        <li>
                            <label class="icr-label">
                                <span class="icr-item type_radio"></span>
                                <span class="icr-hidden"><input class="icr-input" type="radio" name="show" value="2" /></span>
                                <span class="icr-text"><span>360° Панорамы</span> <i>(13)</i></span>
                            </label>
                        </li>
                        <li>
                            <label class="icr-label">
                                <span class="icr-item type_radio"></span>
                                <span class="icr-hidden"><input class="icr-input" type="radio" name="show" value="3" /></span>
                                <span class="icr-text"><span>360° Видео</span> <i>(2)</i></span>
                            </label>
                        </li>
                        <li>
                            <label class="icr-label">
                                <span class="icr-item type_radio"></span>
                                <span class="icr-hidden"><input class="icr-input" type="radio" name="show" value="4" /></span>
                                <span class="icr-text"><span>Фотогалерея</span> <i>(8)</i></span>
                            </label>
                        </li>
                        <li>
                            <label class="icr-label">
                                <span class="icr-item type_radio"></span>
                                <span class="icr-hidden"><input class="icr-input" type="radio" name="show" value="4" /></span>
                                <span class="icr-text"><span>Новости</span> <i>(7)</i></span>
                            </label>
                        </li>
                    </ul>
                </div>

                <div class="search-result">
                    <h3><span>360° Панорамы <span class="num">(13)</span></span></h3>

                    <ul class="gallery-table clearfix">

                        <li>
                            <a href="#" class="gallery-lnk">
                                <img src="images/home_img_01.jpg" alt="" class="img-responsive">
                                <div class="gallery-text">
                                    <span>Бутан. Часть 2. Монастырь Тхангби Лхакханг</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="gallery-lnk">
                                <img src="images/home_img_02.jpg" alt="" class="img-responsive">
                                <div class="gallery-text">
                                    <span>Джакарта, Индонезия</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="gallery-lnk">
                                <img src="images/home_img_03.jpg" alt="" class="img-responsive">
                                <div class="gallery-text">
                                    <span>Сенот Эль-Пит, Мексика</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="gallery-lnk">
                                <img src="images/home_img_04.jpg" alt="" class="img-responsive">
                                <div class="gallery-text">
                                    <span>Рафтинг на реке Замбези, Замбия-Зимбабве</span>
                                </div>
                            </a>
                        </li>

                    </ul>

                    <div class="search-all">
                        <a href="#"><span>Смотреть все результаты 360° Панорам</span> <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>


                <div class="search-result">
                    <h3><span>360° Видео <span class="num">(2)</span></span></h3>

                    <ul class="gallery-table gallery-video clearfix">

                        <li>
                            <a href="#" class="gallery-lnk">
                                <img src="images/home_video_01.jpg" alt="" class="img-responsive">
                                <div class="gallery-text">
                                    <span>Венецианский карнавал. Часть 1</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="gallery-lnk">
                                <img src="images/home_video_02.jpg" alt="" class="img-responsive">
                                <div class="gallery-text">
                                    <span>Водопад Виктория, Замбия - Зимбабве. Часть 1</span>
                                </div>
                            </a>
                        </li>
                    </ul>

                </div>


                <div class="search-result">
                    <h3><span>Фотогалерея <span class="num">(8)</span></span></h3>

                    <div class="search-photo">
                        <div class="flexbin flexbin-margin">
                            <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/home_gallery/img01.jpg" alt="" class="img-responsive">
                                <div class="collage-text">
                                    <b>Центральный парк Нью-Йорк, США</b>
                                    <span>ID 5536</span>
                                </div>
                            </a>
                            <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/home_gallery/img02.jpg" alt="" class="img-responsive">
                                <div class="collage-text">
                                    <b>Центральный парк Нью-Йорк, США</b>
                                    <span>ID 5536</span>
                                </div>
                            </a>
                            <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/home_gallery/img03.jpg" alt="" class="img-responsive">
                                <div class="collage-text">
                                    <b>Центральный парк Нью-Йорк, США</b>
                                    <span>ID 5536</span>
                                </div>
                            </a>
                            <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/home_gallery/img04.jpg" alt="" class="img-responsive">
                                <div class="collage-text">
                                    <b>Центральный парк Нью-Йорк, США</b>
                                    <span>ID 5536</span>
                                </div>
                            </a>
                            <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/home_gallery/img05.jpg" alt="" class="img-responsive">
                                <div class="collage-text">
                                    <b>Центральный парк Нью-Йорк, США</b>
                                    <span>ID 5536</span>
                                </div>
                            </a>
                            <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/home_gallery/img06.jpg" alt="" class="img-responsive">
                                <div class="collage-text">
                                    <b>Центральный парк Нью-Йорк, США</b>
                                    <span>ID 5536</span>
                                </div>
                            </a>
                            <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/home_gallery/img07.jpg" alt="" class="img-responsive">
                                <div class="collage-text">
                                    <b>Центральный парк Нью-Йорк, США</b>
                                    <span>ID 5536</span>
                                </div>
                            </a>
                            <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/home_gallery/img08.jpg" alt="" class="img-responsive">
                                <div class="collage-text">
                                    <b>Центральный парк Нью-Йорк, США</b>
                                    <span>ID 5536</span>
                                </div>
                            </a>
                            <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                                <img src="images/home_gallery/img09.jpg" alt="" class="img-responsive">
                                <div class="collage-text">
                                    <b>Центральный парк Нью-Йорк, США</b>
                                    <span>ID 5536</span>
                                </div>
                            </a>
                        </div>

                    </div>
                    <div class="search-all">
                        <a href="#"><span>Смотреть все результаты Фотогалереи</span> <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>


                <div class="search-result">
                    <h3><span>Новости <span class="num">(7)</span></span></h3>

                    <div class="search-news">

                        <div class="search-news-item">
                            <div class="search-news-date">20 апреля 2013</div>
                            <h4><a href="#">Финальные работы тренинга Визуализация экстерьеров 2013</a></h4>
                            <p>... , Ростов-на-Дону Гречухина Елизавета, <strong>Швейцария</strong>, Женева Беляков Илья, Новокузнецк, Россия ...</p>
                        </div>

                        <div class="search-news-item">
                            <div class="search-news-date">11 июля 2015</div>
                            <h4><a href="#">День открытых дверей Школы дизайна - краски Caparol и экскурсия в дом 1500 метров</a></h4>
                            <p>... 2014 состоялся очередное занятие Школы дизайна в <strong>Швейцарии</strong>. В этот раз оно состояло ...</p>
                        </div>

                        <div class="search-news-item">
                            <div class="search-news-date">29 марта 2016</div>
                            <h4><a href="#">Выступление на конференции успеха о дизайне интерьеров в Швейцарии</a></h4>
                            <p>Станислав Орехов стал приглашенным спикером на очередной конференции успеха в <strong>Швейцарии</strong>. Тема: дизайн интерьера. Подробности - на видео в разделе Лекции и вебинары.</p>
                        </div>

                    </div>

                    <div class="search-all">
                        <a href="#"><span>Смотреть все результаты Новостей</span> <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
