<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?><!-- -->

        <!-- Pagination -->
        <div class="pagination">
            <div class="container-fluid">
                <div class="pagination-back clearfix">
                    <a href="#">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <span>Главная</span>
                </div>

                <ul class="pagination-nav">
                    <li><a href="#">Главная</a></li>
                    <li>360° Видео AirPano</li>
                </ul>
            </div>
        </div><!-- -->

        <section class="main">
            <div class="container">
                <h1>360° Панорамы AirPano</h1>

                <div class="display-bar clearfix">
                    <div class="show-bar clearfix">
                        <div class="display-label">Показывать:</div>
                        <div class="show-bar-nav">
                            <ul class="drop-list clearfix">
                                <li class="sort-table active"><a href="#">таблицей</a></li>
                                <li class="sort-list"><a href="#">списком</a></li>
                                <li class="sort-map"><a href="#">на карте</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sort-nav clearfix">
                        <div class="display-label">Выводить по:</div>
                        <div class="sort-select">
                            <select name="sort">
                                <option value="">популярности</option>
                                <option value="">публикации</option>
                                <option value="">алфавиту</option>
                            </select>
                        </div>
                    </div>
                </div>

                <ul class="gallery-table clearfix">

                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_01.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Бутан. Часть 2. Монастырь Тхангби Лхакханг</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_02.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Джакарта, Индонезия</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_03.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Сенот Эль-Пит, Мексика</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_04.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Рафтинг на реке Замбези, Замбия-Зимбабве</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_05.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Луксор, Египет</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_06.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Улан-Батор, Монголия</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_07.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Сеговия, Испания</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_08.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Замки Луары, Франция. Часть 2</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_01.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Бутан. Часть 2. Монастырь Тхангби Лхакханг</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_02.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Джакарта, Индонезия</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_03.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Сенот Эль-Пит, Мексика</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_04.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Рафтинг на реке Замбези, Замбия-Зимбабве</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_05.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Луксор, Египет</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_06.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Улан-Батор, Монголия</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_07.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Сеговия, Испания</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="gallery-lnk">
                            <img src="images/home_img_08.jpg" alt="" class="img-responsive">
                            <div class="gallery-text">
                                <span>Замки Луары, Франция. Часть 2</span>
                            </div>
                        </a>
                    </li>
                </ul>

                <div class="ajax-load">
                    <span class="ajax-load-icon">
                        <img src="img/icon-download.png" alt="">
                    </span>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
