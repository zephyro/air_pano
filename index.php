<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?><!-- -->
    
    <body>

        <header class="header">

            <!-- Top Bar -->
            <div class="topbar clearfix">
                <div class="container-fluid">
                    <a href="#" class="header-logo">
                        <img src="img/header_logo_xl.png" alt="" class="img-responsive">
                    </a>
                    <span class="btn-nav navbar-toggle" data-target=".navbar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>

                    <nav class="navbar clearfix">
                        <span class="icon-close navbar-toggle"></span>
                        <div class="navbar-search hidden-lg">
                            <input type="text" name="search" class="navbar-input-search" placeholder="Поиск по сайту">
                            <button type="submit" class="btn-send"></button>
                        </div>
                        <ul class="navbar-main">
                            <li class="active"><a href="#">360° Панорамы</a></li>
                            <li><a href="#">360° Видео</a></li>
                            <li><a href="#">Фотогалерея</a></li>
                            <li><a href="#">Получить прайс</a></li>
                        </ul>

                        <div class="navbar-lng clearfix">
                            <span class="lng-text">Выберите язык:</span>
                            <ul class="lng drop-list">
                                <li class="active"><a href="#">Ru</a></li>
                                <li><a href="#">En</a></li>
                            </ul>
                        </div>

                    </nav>
                </div>

            </div>
            <!-- -->

            <!-- Slider -->
            <div class="header-slider">
                <div class="header-slider-item" style="background-image: url('img/header_slider_01.jpg')"></div>
                <div class="header-slider-item" style="background-image: url('img/header_slider_01.jpg')"></div>
                <div class="header-slider-item" style="background-image: url('img/header_slider_01.jpg')"></div>
            </div><!-- -->
            <span class="header-slider-nav prev"></span>
            <span class="header-slider-nav next"></span>

            <!-- Content -->
            <div class="header-content">
                <div class="container">
                    <div class="header-content-logo">
                        <img src="img/header_logo_xl.png" class="img-responsive" alt="">
                    </div>
                    <div class="header-content-text">Виртуальные путешествия вокруг света</div>
                    <div class="header-content-search">
                        <div class="header-search">
                            <form class="form">
                                <input type="text" class="form-search" name="search" placeholder="Например, вулкан ...">
                                <button type="submit" class="btn-search"></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- -->
        </header>

        <section class="section">
            <div class="heading"><span>360° Панорамы</span></div>
            <div class="container">
                <div class="preview-gallery preview-photo clearfix">

                    <ul class="clearfix">
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_img_01.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                    <span>Бутан. Часть 2. Монастырь Тхангби Лхакханг</span>
                                </span>
                            </a>
                        </li>
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_img_02.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                <span>Джакарта, Индонезия</span>
                            </span>
                            </a>
                        </li>
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_img_03.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                <span>Сенот Эль-Пит, Мексика</span>
                            </span>
                            </a>
                        </li>
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_img_04.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                <span>Рафтинг на реке Замбези, Замбия-Зимбабве</span>
                            </span>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="text-center">
                    <a href="#" class="btn">Смотреть все 360° панорамы <span>(358)</span></a>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="heading"><span>360° Видео</span></div>
            <div class="container">
                <div class="preview-gallery preview-video clearfix">

                    <ul class="clearfix">
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_video_01.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                <span>Венецианский карнавал. Часть 1</span>
                            </span>
                            </a>
                        </li>
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_video_02.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                <span>Водопад Виктория, Замбия - Зимбабве. Часть 1</span>
                            </span>
                            </a>
                        </li>
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_video_03.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                <span>Тбилиси, Грузия</span>
                            </span>
                            </a>
                        </li>
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_video_04.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                <span>Венецианский карнавал. Часть 2</span>
                            </span>
                            </a>
                        </li>
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_video_05.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                             <span>Тбилиси, Грузия</span>
                            </span>
                            </a>
                        </li>
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_video_06.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                <span>Водопад Виктория, Замбия - Зимбабве. Часть 1</span>
                            </span>
                            </a>
                        </li>
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_video_07.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                <span>Венецианский карнавал. Часть 2</span>
                            </span>
                            </a>
                        </li>
                        <li class="preview-item">
                            <a href="#" class="item-lnk">
                                <img src="images/home_video_08.jpg" alt="" class="img-responsive">
                                <span class="item-text">
                                <span>Венецианский карнавал. Часть 1</span>
                            </span>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="text-center">
                    <a href="#" class="btn">Смотреть все 360° видео <span>(358)</span></a>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="heading"><span>География съемок</span></div>
            <div class="home-map" id="map"></div>
        </section>

        <section class="section section-gallery">
            <div class="heading"><span>Фотогалерея</span></div>
            <div class="container">
                <div class="home-gallery">

                    <div class="flexbin flexbin-margin">
                        <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                            <img src="images/home_gallery/img01.jpg" alt="" class="img-responsive">
                            <div class="collage-text">
                                <b>Центральный парк Нью-Йорк, США</b>
                                <span>ID 5536</span>
                            </div>
                        </a>
                        <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                            <img src="images/home_gallery/img02.jpg" alt="" class="img-responsive">
                            <div class="collage-text">
                                <b>Центральный парк Нью-Йорк, США</b>
                                <span>ID 5536</span>
                            </div>
                        </a>
                        <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                            <img src="images/home_gallery/img03.jpg" alt="" class="img-responsive">
                            <div class="collage-text">
                                <b>Центральный парк Нью-Йорк, США</b>
                                <span>ID 5536</span>
                            </div>
                        </a>
                        <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                            <img src="images/home_gallery/img04.jpg" alt="" class="img-responsive">
                            <div class="collage-text">
                                <b>Центральный парк Нью-Йорк, США</b>
                                <span>ID 5536</span>
                            </div>
                        </a>
                        <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                            <img src="images/home_gallery/img05.jpg" alt="" class="img-responsive">
                            <div class="collage-text">
                                <b>Центральный парк Нью-Йорк, США</b>
                                <span>ID 5536</span>
                            </div>
                        </a>
                        <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                            <img src="images/home_gallery/img06.jpg" alt="" class="img-responsive">
                            <div class="collage-text">
                                <b>Центральный парк Нью-Йорк, США</b>
                                <span>ID 5536</span>
                            </div>
                        </a>
                        <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                            <img src="images/home_gallery/img07.jpg" alt="" class="img-responsive">
                            <div class="collage-text">
                                <b>Центральный парк Нью-Йорк, США</b>
                                <span>ID 5536</span>
                            </div>
                        </a>
                        <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                            <img src="images/home_gallery/img08.jpg" alt="" class="img-responsive">
                            <div class="collage-text">
                                <b>Центральный парк Нью-Йорк, США</b>
                                <span>ID 5536</span>
                            </div>
                        </a>
                        <a href="images/gallery_album_01.jpg" class="btn-modal-image" data-fancybox="gallery" data-caption="<div class='album-list-name'>Рио-де-Жанейро, Бразилия</div> <span class='album-list-id'>ID 1223</span>">
                            <img src="images/home_gallery/img09.jpg" alt="" class="img-responsive">
                            <div class="collage-text">
                                <b>Центральный парк Нью-Йорк, США</b>
                                <span>ID 5536</span>
                            </div>
                        </a>
                    </div>

                </div>
                <div class="text-center">
                    <a href="#" class="btn">Смотреть все фотографии <span>(1356)</span></a>
                </div>
            </div>
        </section>

        <section class="section section-service">
            <div class="heading"><span>Наши услуги</span></div>
            <div class="container">
                <ul class="home-service clearfix">
                    <li>
                        <div class="service-image">
                            <img src="img/home_service_01.jpg" alt="" class="img-responsive">
                        </div>
                        <h4>Панорамная 360° съемка</h4>
                        <ul class="service-info">
                            <li><a href="#">360° Панорама</a></li>
                            <li><a href="#">360° Видео</a></li>
                            <li><a href="#">Фото</a></li>
                            <li><a href="#">360° Таймлапс</a></li>
                        </ul>
                        <a href="#" class="btn btn-modal">Отправить заявку</a>
                    </li>
                    <li>
                        <div class="service-image">
                            <img src="img/home_service_02.jpg" alt="" class="img-responsive">
                        </div>
                        <h4>Продажа контента</h4>
                        <ul class="service-info">
                            <li><a href="#">Виртуальный тур</a></li>
                            <li><a href="#">Презентация</a></li>
                            <li><a href="#">Приложения</a></li>
                            <li><a href="#">Фотобанк</a></li>
                        </ul>
                        <a href="#" class="btn btn-modal">Отправить заявку</a>
                    </li>
                    <li>
                        <div class="service-image">
                            <img src="img/home_service_03.jpg" alt="" class="img-responsive">
                        </div>
                        <h4>Создание выставочных инсталляций</h4>
                        <ul class="service-info">
                            <li><a href="#">Панорамный кинотеатр</a></li>
                            <li><a href="#">Сенсорная панель</a></li>
                            <li><a href="#">VR Очки</a></li>
                            <li><a href="#">Бинокуляр</a></li>
                        </ul>
                        <a href="#" class="btn btn-modal">Отправить заявку</a>
                    </li>
                </ul>
            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?><!-- -->

        <!-- Script -->
        <?php include('inc/sctipt.inc.php') ?><!-- -->

    </body>
</html>
